local mee = microexpansion_extended
local me = microexpansion

local S = mee.translator

-- Translate then formspec-escape the translated string nya
local function fsS(str, ...)
    return minetest.formspec_escape(S(str, ...))
end

-- Remove a fixed number of items 
-- from the ME network and into an
-- internal inventory nya
--
-- This maintains the number of items.
--
-- If digilines is installed, then you
-- can set the number and type of the items
-- to maintain nya

-- Test if there is a specified filter item
-- in the inventory nya
-- if there is none, return nil
local function has_filter_item(local_inventory)
    local stack = local_inventory:get_stack("filter", 1)
    if stack:get_name() ~= "" and stack:is_known() then return stack end
end


-- Rescan from the network and test for
-- * if we don't have enough items
-- * and if not, try to take as many from the
--   network as possible to fill up the require
--   ment nyaaaaa
local function try_move_to_local_inventory(pos)
    local net = me.get_connected_network(pos)
    if not net then return end
    local net_inventory = net:get_inventory()
    
    local local_meta = minetest.get_meta(pos)
    -- Only transfer if enabled
    if local_meta:get_int("enabled") == 0 then return end
    local local_inventory = local_meta:get_inventory()
    local match_item_meta = local_meta:get_int("match_item_meta") ~= 0
    local filter_stack = has_filter_item(local_inventory)
    if not filter_stack then return end
    local desired_count = local_meta:get_int("item_count")

    -- Current number of matching items in the inventory nya
    local actual_count = 0
    local local_inventory_list = local_inventory:get_list("main")
    for _, stack in ipairs(local_inventory_list) do
        if not stack:is_empty() then
            -- Check name
            local matches = stack:get_name() == filter_stack:get_name()
            matches = matches and 
                (not match_item_meta or stack:get_meta():equals(filter_stack:get_meta()))
            if matches then actual_count = actual_count + stack:get_count() end
        end
    end


    -- If the filter is satisfied then return, of course.
    if actual_count >= desired_count then return end

    local needed = desired_count - actual_count
    
    -- Get the valid stacks to take from the net inventory list nya
    --
    -- Because of metadata issues with overflowing inventories we do NOT allow to predetermine
    -- the amount of items to take from the inventory nyaa. Essentially if you can't take 
    -- as much from one stack as expected, but you can take from others more than calculated,
    -- then you can underutilise the latter stacks nya.
    
    local allowed_stacks = {}
    local net_invlist = net_inventory:get_list("main")
    for idx, stack in ipairs(net_invlist) do
        if not stack:is_empty() and stack:is_known()
            and stack:get_name() == filter_stack:get_name()
            and (not match_item_meta or stack:get_meta():equals(filter_stack:get_meta())) 
        then
            allowed_stacks[idx] = true
        end
    end

    -- Now mess with inventory list...
    -- Note we have to mess around with stack sizes because ME is screwey with them >.< nya
    

    -- This function attempts to insert a stack which may be larger than max stack size and
    -- returns the new stack to be put back in place nya
    local function transfer_from_me_stack(me_stack, amount_to_take)
        local max_stack_size = me_stack:get_stack_max()
        local left_to_take = amount_to_take
        while left_to_take > 0 do
            local taken_stack = me_stack:take_item(math.min(max_stack_size, left_to_take))
            local taken_amount = taken_stack:get_count() -- The actual amount taken from 
            -- the ME stack nya
            
            -- The amount not inserted that was taken - this needs to be dumped back
            -- into the ME inventory - if not empty we want to return it
            local not_inserted = local_inventory:add_item("main", taken_stack)
            if not not_inserted:is_empty() then 
                -- Act like this was never taken from the main stack nya
                --
                -- There was no room for this so we return it as leftovers
                me_stack:set_count(not_inserted:get_count() + me_stack:get_count()) 
                return me_stack
            end
            
            -- If there is nothing left in the me stack then return it nya
            if me_stack:is_empty() then return me_stack end

            -- the amount not inserted was empty, so we have to remove the amount taken
            -- from the amount we want to take from the stack nya
            left_to_take = left_to_take - taken_amount
        end
        -- Reached the end, there was something left in the ME stack nya, return it...
        return me_stack
    end

    -- Now attempt to dump all the stacks in the ME inventory table into the local inventory
    
    local total_amount_taken = 0

    for valid_idx, is_valid in pairs(allowed_stacks) do
        if is_valid and needed > 0 then
            local old_me_stack = net_invlist[valid_idx] 
            local old_me_count = old_me_stack:get_count()

            net_invlist[valid_idx] = transfer_from_me_stack(old_me_stack, needed)
            local amount_taken = old_me_count - net_invlist[valid_idx]:get_count()
            total_amount_taken = total_amount_taken + amount_taken
            needed = needed - amount_taken
        end
    end

    -- If we actually took anything, then notify the ME network nya
    if total_amount_taken > 0 then
        net_inventory:set_list("main", net_invlist)
        me.send_event(pos, "items", {net = net})
    end
end




local main_inv_width = 5
local main_inv_height = 2



-- Regenerate the formspec from the contents
-- of the meta of the remover nya
--
-- Some of this is taken from the pipeworks mod
local function regenerate_formspec(meta)
    local fs = "formspec_version[2]"
    fs = fs .. "size[14,12]"

    -- Filter...
    fs = fs .. "list[context;filter;1,1;1,1;]"
    fs = fs .. "field[2.5,1;5,1;item_count;" .. fsS("Item Count") .. ";${item_count}]"
    fs = fs .. "field_close_on_enter[item_count;false]"

    -- Check meta
    if meta:get_int("match_item_meta") ~= 0 then
        fs = fs .. "checkbox[8,1;match_item_meta;" .. fsS("Match on Item Metadata") .. ";true]"
    else
        fs = fs .. "checkbox[8,1;match_item_meta;" .. fsS("Match on Item Metadata") .. ";false]"
    end

    -- Enabled
    if meta:get_int("enabled") ~= 0 then
        fs = fs .. "checkbox[8,2;enabled;" .. fsS("Enable the ME Remover") .. ";true]"
    else
        fs = fs .. "checkbox[8,2;enabled;" .. fsS("Enable the ME Remover") .. ";false]"
    end

    -- Save button
    --
    -- When this is clicked we will actually pay attention to new values ;p nya
    fs = fs .. "button[8,3;4,1;save;" .. fsS("Save Settings") .. "]"

    -- If we have digilines then provide channel field
    if mee.digilines then
        fs = fs .. "field[1,3;6.5,1;channel;" .. fsS("Digiline Control Channel") .. ";${channel}]"
        fs = fs .. "field_close_on_enter[channel;false]"
    end

    -- Main inventory nya
    fs = fs .. "list[context;main;1,4.5;10,1.5;]" 
    fs = fs .. "list[current_player;main;2,6.5;8,4;]" 
    fs = fs .. "listring[]"
    meta:set_string("formspec", fs)
end

-- Update the filter from some arbitrary nonempty stack (with metadata etc.)
--
-- If stack is nil, this empties the filter nya
local function update_filter_from(meta, stack)
    local inv = meta:get_inventory()
    if stack and not stack:is_empty() and stack:is_known() then
        stack:set_count(1)
        inv:set_stack("filter", 1, stack) 
    else
        inv:set_stack("filter", 1, ItemStack(nil))
    end
end

local digilines_api = S([[
MicroExpansion Remover - DIGILINES API DOCUMENTATION
* message = {m="set:enabled", v=true or false (or other boolean-ish values)} 
   -> enable or disable the ME Remover. v defaults to false/nil
* message = {m="get:enabled"} 
   -> send back a message of the form {m="state:enabled", v=true or false} indicating 
   whether or not the remover is enabled

* message = {m="set:item_metadata_match", v=true or false (or other boolean-ish values)}
   -> enable or disable matching based on item metadata. v defaults to false/nil
* message = {m="get:item_metadata_match"}
   -> send back a message of the form {m="state:item_metadata_match", v = true or false}
   indicating if the remover is matching on item metadata.

* message = {m="get:item_count"} -> send back a message of the form {m="state:item_count", v=number}
  indicating the number of items this is attempting to maintain in it's inventory.
* message = {m="get:item_name"} -> send back a message of the form {m="state:item_name", v=item name or nil}
  The item name is the internal name (like `default:dirt`). This returns the item type that is being filtered for.
  It returns nil if the filter is currently empty
* message = {m="get:item_metadata"} -> send back a message of the form {m="state:item_metadata", v=table of metadata k/v or nil}.
  This gets the metadata table of the item being filtered for if any. If the item has no metadata or if there is no filtered item
* message = {m="set:item", v=<see below>}
   This method results in setting the item being filtered for. The value sent over is a complete
   table containing the following entries:
   * item_name: A string containing the internal item name of the new filter.
     If not registered will vomit back the help message
   * item_count: A number (>=0, floored to int) that indicates the amount to attempt to 
     keep extracted from the ME network. If not specified, defaults to 1.
   * (optional) item_metadata: A key/value table where the keys and values are exclusively strings
      (this will be filtered out). If this is not set then the metadata will be set to empty. If it
      is set then the filter item will be given the specified metadata, which may be useful if 
      item_metadata_match was set.

      If optional keys are present but of the wrong type, this will result in an error as well.

All get: methods on the digilines API allow an optional session_key=<object> thet will simply
be sent back in the response if present.

Sending any other message than a table with the given methods above and the non-optional keys
will result in this help being sent (as a string) instead...
]])

-- Methods without sending session keys.
--
-- Input here should return a result message. If it is a table, then any session 
-- keys will be automatically added. If it is -1 then return the help message.
-- If it is nil then it is nothing nya
local methods = {
    ["set:enabled"] = function (meta, v)
        if v then
            meta:set_int("enabled", 1) 
        else
            meta:set_int("enabled", 0)
        end
    end,
    ["get:enabled"] = function (meta, _)
        local res = false
        if meta:get_int("enabled") ~= 0 then res = true end
        return {m="state:enabled", v=res}
    end,


    ["set:item_metadata_match"] = function(meta, v)
        if v then
            meta:set_int("match_item_meta", 1)
        else
            meta:set_int("match_item_meta", 0)
        end
    end,
    ["get:item_metadata_match"] = function(meta, _)
        local res = false
        if meta:get_int("match_item_meta") then res = true end
        return {m="state:item_metadata_match", v=res}
    end,

    ["get:item_count"] = function(meta, _)
        return {m="state:item_count", v=meta:get_int("item_count")}
    end,
    ["get:item_name"] = function(meta, _)
        local stack = has_filter_item(meta:get_inventory())
        if stack then return {m="state:item_name", v=stack:get_name()} end
        return {m="state:item_name", v=nil}
    end,
    ["get:item_metadata"] = function(meta, _)
        local stack = has_filter_item(meta:get_inventory())
        if not stack then return {m="state:item_metadata", v=nil} end
        -- Have a stack, get the metadata fields nya
        local stack_meta = stack:get_meta()
        if not stack_meta then return {m="state:item_metadata", v=nil} end
        -- Get the actual meta table. If none, then it is treated like having no metadata nya
        local stack_meta_table = stack_meta:to_table()
        if (not stack_meta_table) or (not stack_meta_table.fields) then
            return {m="state:item_metadata", v=nil}
        end
        return {m="state:item_metadata", v=table.copy(stack_meta_table.fields)}
    end,
    ["set:item"] = function(meta, v)
        if not v then return -1 end
        if type(v) ~= "table" then return -1 end
        if type(v.item_name) ~= "string" then return -1 end
        if not minetest.registered_items[v.item_name] or v.item_name == "" then return -1 end
        local new_item_name = v.item_name
        
        -- Test all the stuff for new counts nya
        local new_item_count = 1
        if v.item_count ~= nil then
            if type(v.item_count) ~= "number" then return -1 end
            if v.item_count < 0 then return -1 end
            new_item_count = math.floor(v.item_count)
        end
        
        local new_item_metadata = nil
        -- Test all the stuff from metadata.
        if v.item_metadata ~= nil then
            if type(v.item_metadata) ~= "table" then return -1 end
            -- Deep copy and filter out any keys or values that aren't strings
            new_item_metadata = {}
            for key, val in pairs(v.item_metadata) do
                if type(key) == "string" and type(val) == "string" then
                    new_item_metadata[key] = val
                end
            end
        end

        -- Now actually set the filter nyaaa
        -- STACK
        local new_stack = ItemStack(new_item_name)
        new_stack:set_count(1)
        if new_item_metadata then
            new_stack:get_meta():from_table(new_item_metadata)
        end
        update_filter_from(meta, new_stack)

        -- ITEM COUNT nya
        meta:set_int("item_count", new_item_count)
    end
}



minetest.register_node("microexpansion_extended:remover", {
    description = S("ME Item Remover"),
    -- Apparently ME has some textures from AE2 MC mod, lets use them ;3 nya
    tiles = {"me_interface.png"},
    groups = {cracky=2, oddly_breakable_by_hand=2, me_connect=1},
    on_construct = function (pos)
        -- Create the inventories properly nya
        local meta = minetest.get_meta(pos)
        
        -- Enabled is a (0 = false, !0 = true)
        -- boolean entry
        meta:set_int("enabled", 1)

        -- Digilines channel :)
        if mee.digilines then 
            meta:set_string("channel", "ME REMOVER" .. minetest.pos_to_string(pos))
        end

        -- Holds whether or not to match metadata
        -- nya. Boolean in the same style as 
        -- above
        meta:set_int("match_item_meta", 0)

        -- The number of items to attempt to keep in the inventory 
        -- nya. This is what is actually used in the filter nya ^.^
        meta:set_int("item_count", 1)

        local inv = meta:get_inventory()

        -- Inventory where items are output nya
        inv:set_size("main", main_inv_width * main_inv_height)
        -- Holds the filter item nya.
        -- This uses ghost items, and some of 
        -- the code for managing that is taken 
        -- from https://gitlab.com/VanessaE/pipeworks/-/blob/master/autocrafter.lua
        -- (licensed under LGPLv3+)
        inv:set_size("filter", 1*1)

        -- Connect to the network. Even if nil we still? want to
        -- put a connection event.
        --
        -- Taken from https://github.com/theFox6/microexpansion/blob/master/modules/storage/terminal.lua
        -- (licensed under MIT, all the code for interfacing with
        -- ME networks is) nya
        local net = me.get_connected_network(pos)
        me.send_event(pos, "connect", {net = net})

        -- Regenerate our formspec nya
        regenerate_formspec(meta)
        minetest.get_node_timer(pos):start(1.5)

    end,
    can_dig = function (pos, player)
        local meta = minetest.get_meta(pos)
        local inv = meta:get_inventory()
        return inv:is_empty("main")
    end,
    after_destruct = function(pos)
        me.send_event(pos, "disconnect")
    end,
    -- Receiving formspec updates nya
    on_receive_fields = function(pos, _formname, fields, _sender)
        -- Checkboxes don't get sent properly with everything else, so we have to 
        -- do them before the check nya. In fact they will be nil and the sensible 
        -- code to handle them will result in always being set false >.< nya
        local meta = minetest.get_meta(pos) 
        
        if fields.enabled == "true" then
            minetest.log("action", "[ME-Ex] ME Remover enabled at " .. minetest.pos_to_string(pos))
            meta:set_int("enabled", 1)
        elseif fields.enabled == "false" then
            minetest.log("action", "[ME-Ex] ME Remover disabled at " .. minetest.pos_to_string(pos))
            meta:set_int("enabled", 0)
        end
        if fields.match_item_meta == "true" then
            minetest.log("action", "[ME-Ex] ME Remover now matching metadata at " .. minetest.pos_to_string(pos))
            meta:set_int("match_item_meta", 1)
        elseif fields.match_item_meta == "false" then
            minetest.log("action", "[ME-Ex] ME Remover no longer matching metadata at " .. minetest.pos_to_string(pos))
            meta:set_int("match_item_meta", 0)
        end

        if fields.item_count and fields.save then
            local num = tonumber(fields.item_count)
            if not type(num) == "number" and num >= 0 then return end
            meta:set_int("item_count", math.floor(num))
        end
        if mee.digilines and type(fields.channel) == "string" and fields.save then
            meta:set_string("channel", fields.channel)
        end
        regenerate_formspec(meta)
        try_move_to_local_inventory(pos)
    end,

    -- Some inventory manipulation here, this uses code from pipeworks nya
    allow_metadata_inventory_move = function(pos, from_list, from_index, to_list, to_index, cnt, player)
        -- If the target is the filter stack then set it.
        local meta = minetest.get_meta(pos)
        local inv = meta:get_inventory()
        local stack_transfer_wanted = inv:get_stack(from_list, from_index) 
        -- Set the filter
        if to_list == "filter" then
            update_filter_from(meta, stack_transfer_wanted)
            -- Need to rescan here
            try_move_to_local_inventory(pos)
            return 0
        elseif from_list == "filter" then
            -- clear out the filter nya
            update_filter_from(meta, nil)
            return 0
        end

        -- This wasn't a transfer to or from filter, simply dump out nya
        return cnt
    end,

    -- When the player tries to remove from the filter...
    allow_metadata_inventory_take = function(pos, from_list, idx, stack, player)
        if from_list == "filter" then 
            local meta = minetest.get_meta(pos)
            update_filter_from(meta, nil)
            return 0
        end
        return stack:get_count()
    end,

    -- Finally, when they try and add something in nya
    allow_metadata_inventory_put = function(pos, to_list, idx, stack, player)
        if to_list == "filter" then
            local meta = minetest.get_meta(pos)
            update_filter_from(meta, stack)
            try_move_to_local_inventory(pos)
            return 0
        end
        return stack:get_count()
    end,
    -- microexpansion thing nya
    me_update = function(pos, _, event)
        try_move_to_local_inventory(pos)
    end,
    -- Timer since i'm really not sure about ME events (in particular the item events)
    on_timer = function(pos, elapsed)
        try_move_to_local_inventory(pos)
        return true
    end,
    
    -- Digilines stuff. Partially taken from pipeworks autocrafter :) nya
    --
    -- Note that obviously this won't do anything if digiline isn't installed ;p
    digiline = {
        -- Sensors, to me this would be "transmitter" but i didn't design the api ;p
        receptor = {},
        effector = {action = function(pos, node, channel, msg)
            if not mee.digilines then return end -- doesn't make sense without it anyhow.
            local meta = minetest.get_meta(pos)
            if channel ~= meta:get_string("channel") then return end
            if type(msg) ~= "table" or not methods[msg.m] then 
                mee.digilines.receptor_send(
                    pos, mee.digilines.rules.default, channel, digilines_api
                ) 
            end
            local method_result = methods[msg.m](meta, msg.v)

            -- Results - add session key as well nya
            if type(method_result) == "table" then
                method_result.session_key = msg.session_key
            elseif type(method_result) == "number" and method_result == -1 then
                -- set to help string instead nya
                method_result = digilines_api
            else  -- Other types shouldn't exist here so we set to nil ^.^
                method_result = nil
            end

            if method_result then
                mee.digilines.receptor_send(pos, mee.digilines.rules.default, channel, method_result)
            end
        end}
    },
    -- Pipeworks... we just take the main inventory :) nya
    tube = {
        input_inventory = "main"
    }

})

minetest.register_craft({
    output = "microexpansion_extended:remover 3",
    recipe = {
        {"default:steel_ingot", "microexpansion:cable", "default:steel_ingot"},
        {"basic_materials:silicon", "microexpansion:machine_casing", "default:mese_crystal"},
        {"default:steel_ingot", "microexpansion:cable", "default:steel_ingot"}
    }
})









-- microexpansion-extended
-- Copyright (C) 2021  sapient_cogbag <sapient_cogbag@protonmail.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.
